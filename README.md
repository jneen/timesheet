# README

Basic sample timesheet application!

Stock rails setup using postgres. Get rails and the db set up and run `rails db:seed`. Available publicly at http://jneen-timesheet.herokuapp.com/. (Might be slow to load due to heroku free tier dynos going to sleep!)

Notes:

* Given the 48-hour time limit, this was built as I would build a product in a Discovery or Alpha phase - light on features, mostly exploring different interface options.

* Were this to move to a more serious codebase we'd want to work with a product team to nail down exactly the set of features we're looking for, and add tests.

* That's right: **No, there are no tests**. This is because I am mostly using glue code, and, again, Alpha. Were I to add tests, I'd start with integration tests: I've worked with cucumber/selenium in the past, though I've heard promising things about cypress. Should we need to add more in-depth business logic than is in this app, I would test with rspec.

* Additionally, I went with the simplest schema design possible - users only exist in their names, and there is exactly one table. There is no login and no user options: just clocking in and clocking out. On the control panel page, an option is given to clock in a new user. A move to the next phase would probably require a separate user model, but in reality, it's likely that already exists and would need integration built. Failing that, I opted for the simple approach for this app.

* The date/time input needs serious design work (and possibly research). It's using the default HTML5 form controls, which while nice and accessible, need a lot more styling work than I've put into them.

* There is no general edit feature - only a "forgot to clock in" patching feature. That's something that would need to be added. Ideally it would happen inline on the index page with javascript.

* Error reporting is sparse. I've made the choice to be very light on validation, with the idea that in a real world setting, for timekeeping data, invalid data is something that needs to be fixed post-hoc, not thrown away. The "patch irregularity" functionality is an example of this. However, there are still some error cases that need to be reported to the end user, and that is not yet set up.

* The app is based on two intersecting use cases: A single panel for everyone to clock in/out (at /clocks/new), and an admin interface to view and manage the timesheet (at /clocks, /clocks?name=somebody, /clocks?scope=all). This would require many, many more navigation options in a full-blown application.

* The app is built for a single deployment in a single timezone. Some product work would be required to figure out how the app should be treated across timezones. For example, if someone clocked out very late on Tuesday in BC (say, after 9pm), it might already be Wednesday in Ontario, and we need to decide whether that counts as "on Tuesday." Likely the correct solution is to store the timezone offset alongside the "when" timestamp, so that the date can be determined locally to where the event happened.
