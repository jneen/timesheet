module ApplicationHelper
  def clock_symbol(clock)
    return '&rarr;'.html_safe if clock.in?
    return '&larr;'.html_safe if clock.out?
  end

  def clock_form(direction, name=nil, &b)
    dir_word = direction == Clock::IN ? 'in' : 'out'


    params = { model: Clock.new, class: %W(clock-form clock-#{dir_word}) }
    form_with params do |form|
      concat form.hidden_field(:direction, value: direction)

      if name
        concat form.hidden_field(:name, value: name)
      end

      yield form
    end
  end

  # Unified time format for the app
  # weekday, day-num month-name year, hour:minute AM
  DATE_FORMAT  = '%a, %d %b %Y'
  TIME_FORMAT = '%l:%M %P'
  DATETIME_FORMAT = "#{DATE_FORMAT}, #{TIME_FORMAT}"

  def format_datetime(datetime) datetime.strftime(DATETIME_FORMAT) end
  def format_date(date) date.strftime(DATE_FORMAT) end
  def format_time(time) time.strftime(TIME_FORMAT) end

  # days in reverse order (starting from today going back), but
  # times in normal order (starting from morning)
  def each_day(clocks, &b)
    clocks.group_by { |c| c.when.beginning_of_day }.sort.reverse_each do |(date, clocks)|
      yield date, clocks
    end
  end

  def direction_word(direction)
    case direction
    when Clock::IN then 'in'
    when Clock::OUT then 'out'
    end
  end

  def opposite_direction(direction)
    case direction
    when Clock::IN then Clock::OUT
    when Clock::OUT then Clock::IN
    end
  end

  def direction_bubble(direction)
    word = direction_word(direction)
    content_tag(:span, word, class: "direction direction-#{word}")
  end

  # Collect the set of clock ids for which the direction was
  # already set - for example, clocking in when the person
  # is already clocked in, or out when they are already out.
  # This allows us to insert mini-forms for a manager to
  # correct the irregularity. I've chosen this approach over
  # validation, because even when such an irregularity happens,
  # it's better to record the data anyways and allow for a fix
  # later then to just reject on the spot.
  def irregular_ids(clocks)
    status = Hash.new { |h, k| h[k] = Clock::OUT }
    irregular = Set.new

    clocks.sort_by(&:when).each do |clock|
      current_status, status[clock.name] = status[clock.name], clock.direction

      irregular << clock.id if current_status == clock.direction
    end

    irregular
  end
end
