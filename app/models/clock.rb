class Clock < ApplicationRecord
  IN = 0
  OUT = 1

  def inspect
    type = self.in? ? 'in' :
           self.out? ? 'out' :
           'err'

    "#<Clock[#{type}] #{self.name}@#{self.when.inspect}>"
  end

  validates_presence_of :name
  validates_inclusion_of :direction, in: [IN, OUT]

  def in?;  self.direction == IN;  end
  def out?; self.direction == OUT; end

  def direction_word
    case direction
    when IN then 'in'
    when OUT then 'out'
    end
  end

  def self.for(name)
    where(name: name).order(when: :asc)
  end

  # [jneen] interpret the timestamp locally: if we want Tuesday,
  # we mean Tuesday "here" - eventually clocks themselves should
  # store a tz offset, which should be passed in from the browser.
  def when
    self[:when] && self[:when].localtime
  end

  def self.day(date)
    where(when: date.localtime.all_day).order(when: :asc)
  end

  def self.all_names
    distinct(:name).order(name: :asc).pluck(:name)
  end

  def self.latest(name)
    where(name: name).order(when: :desc).first
  end
end
