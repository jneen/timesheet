class ClocksController < ApplicationController
  def index
    @clocks = case params[:scope]
    when 'name'
      Clock.for(params[:name])
    when 'all'
      Clock.all
    when nil, 'today'
      Clock.day(Time.now)
    end

    @irregular = irregular_ids(@clocks)
  end

  def new
    # [jneen] TODO XXX: This is an n+1 query, but fixing it will require
    # dropping to SQL. Evaluate the need for performance vs db agnosticity
    @latest = Clock.all_names.map(&Clock.method(:latest))
    @clock = Clock.new
  end

  def create
    p params

    clock = params[:clock]

    Clock.create do |c|
      c.name = clock[:name]
      c.when = interpret_timestamp(clock.fetch(:when) { Time.now })
      c.direction = clock[:direction]
    end

    redirect_back fallback_location: '/clocks/new'
  end

  def destroy
    p :params => params
    clock = Clock.find(params[:id])
    clock.destroy
  rescue ActiveRecord::RecordNotFound
    # pass - destroying a nonexistent record is a noop
  ensure
    redirect_back fallback_location: '/'
  end

private
  def interpret_timestamp(value)
    case value
    when Time, DateTime then value
    when Hash, ActionController::Parameters
      Time.strptime("#{value[:date]}-#{value[:time]}", '%Y-%m-%d-%H:%M')
    else
      raise "invalid time #{value.inspect}"
    end
  end
end
