window.Clock = (() => {
  const state = {}

  const handleSubmit = (evt) => {
    for (let i = 0; i < state.forms.length; i += 1) {
      const submitButton = state.forms[i].querySelector('input[type=submit]')
      submitButton.enabled = false
    }
  }

  const init = () => {
    state.forms = document.getElementsByTagName('form')

    for (let i = 0; i < state.forms.length; i += 1) {
      state.forms[i].addEventListener('submit', handleSubmit)
    }
  }

  return {
    init,
  }
})()

window.addEventListener('load', Clock.init)
