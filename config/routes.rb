Rails.application.routes.draw do
  get '/', to: redirect('/clocks')

  resources :clocks
end
