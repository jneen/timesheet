class CreateClocks < ActiveRecord::Migration[6.0]
  def change
    create_table :clocks do |t|
      t.string   :name, :null => false
      t.datetime :when, :null => false
      t.integer  :direction, :null => false
      t.timestamps
    end

    add_index :clocks, [:name, :when]
    add_index :clocks, :when
  end
end
