# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Clock.create(name: 'jneen', when: 3.days.ago.beginning_of_day + 8.hours,  direction: Clock::IN)
Clock.create(name: 'jneen', when: 3.days.ago.beginning_of_day + 16.hours, direction: Clock::OUT)
Clock.create(name: 'alice', when: 3.days.ago.beginning_of_day + 9.hours,  direction: Clock::IN)
Clock.create(name: 'alice', when: 3.days.ago.beginning_of_day + 17.hours, direction: Clock::OUT)
Clock.create(name: 'bob',   when: 3.days.ago.beginning_of_day + 7.hours,  direction: Clock::IN)
Clock.create(name: 'bob',   when: 3.days.ago.beginning_of_day + 15.hours, direction: Clock::OUT)

Clock.create(name: 'jneen', when: 2.days.ago.beginning_of_day + 8.hours,  direction: Clock::IN)
Clock.create(name: 'jneen', when: 2.days.ago.beginning_of_day + 16.hours, direction: Clock::OUT)
Clock.create(name: 'alice', when: 2.days.ago.beginning_of_day + 9.hour,   direction: Clock::IN)
Clock.create(name: 'alice', when: 2.days.ago.beginning_of_day + 12.hours, direction: Clock::OUT)
# alice forgot to clock back in after lunch
Clock.create(name: 'alice', when: 2.days.ago.beginning_of_day + 21.hours,  direction: Clock::OUT)
Clock.create(name: 'bob',   when: 2.days.ago.beginning_of_day + 7.hours,   direction: Clock::IN)
Clock.create(name: 'bob',   when: 2.days.ago.beginning_of_day + 15.hours,  direction: Clock::OUT)

Clock.create(name: 'jneen', when: 1.day.ago.beginning_of_day + 4.hours, direction: Clock::IN)
Clock.create(name: 'alice', when: 1.day.ago.beginning_of_day + 5.hours, direction: Clock::IN)
Clock.create(name: 'alice', when: 1.day.ago.beginning_of_day + 16.hours, direction: Clock::OUT)
Clock.create(name: 'bob',   when: 1.day.ago.beginning_of_day + 18.hours - 5.minutes, direction: Clock::IN)
